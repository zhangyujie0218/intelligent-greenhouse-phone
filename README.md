# 智能温室_phone

#### 介绍
智能温室项目手机APP端



#### 使用说明

1.  需要安装服务端和web端并启动
2.  配置项目中的请求地址


# 服务端地址:[https://gitee.com/zhangyujie0218/intelligent-greenhouse-service](https://gitee.com/zhangyujie0218/intelligent-greenhouse-service)
# web端地址:[https://gitee.com/zhangyujie0218/intelligent-greenhouse-vue](https://gitee.com/zhangyujie0218/intelligent-greenhouse-vue)
